
"""
Contributors can be viewed at:
http://svn.secondlife.com/svn/linden/projects/2008/pyogp/lib/base/trunk/CONTRIBUTORS.txt 

$LicenseInfo:firstyear=2008&license=apachev2$

Copyright 2009, Linden Research, Inc.

Licensed under the Apache License, Version 2.0.
You may obtain a copy of the License at:
    http://www.apache.org/licenses/LICENSE-2.0
or in 
    http://svn.secondlife.com/svn/linden/projects/2008/pyogp/lib/base/LICENSE.txt

$/LicenseInfo$
"""

from uuid import UUID

from llbase import lluuid

# pyogp
from pyogp.lib.client.exc import ResourceError, ResourceNotFound
from pyogp.lib.client.enums import SortOrder

# pyogp
from .core import InventoryManager, logger, InventoryFolder, InventoryItem

FETCH_INVENTORY_CAP_NAME = 'FetchInventory2'
FETCH_INVENTORY_DESCENDENTS_CAP_NAME = 'FetchInventoryDescendents2'
FETCH_LIBRARY_CAP_NAME = 'FetchLib2'
FETCH_LIBRARY_DESCENDENTS_CAP_NAME = 'FetchLibDescendents2'

FOLDER_LLSD_ATTR_MAP = {
    'category_id': 'id',
    'folder_id': 'id',
    'parent_id': 'parent_id',
    'type': 'type',
    'type_default': 'type',
    'name': 'name',
    'descendents': 'descendents',
    'version': 'version',
    'owner_id': 'owner_id',
}

ITEM_LLSD_ATTR_MAP = {
    'item_id': 'id',
    'parent_id': 'parent_id',
    #'permissions': Handled Specially,
    #'sale_info': handled specially
    #'shadow_id': ignored; legacy
    'asset_id': 'asset_id',
    'linked_id': 'linked_id',
    'type': 'type',
    'inv_type': 'inv_type',
    'flags': 'flags',
    'name': 'name',
    'desc': 'description',
    'created_at': 'creation_date',
}

class AISv2Client(object):
    """
    Implements AISv2, currently the standard bulk-inventory fetch client as of viewer 5.0
    AISv2 is a read-only inventory protocol; it cannot modify inventory
    """

    def __init__(self, fetchCapability, fetchDescendentsCapability):
        self.fetchCapability = fetchCapability
        self.fetchDescendentsCapability = fetchDescendentsCapability

    def sendFetchDescendents(self, folders, sort_order = SortOrder.Name,
            fetch_folders = True, fetch_items = True):
        request = {'folders': [{
            'folder_id': str(folder.id),
            'owner_id': folder.owner_id,
            'sort_order': sort_order,
            'fetch_folders': fetch_folders,
            'fetch_items': fetch_items,
        } for folder in folders]}
        return self.fetchDescendentsCapability.POST(request)

    def fetchDescendents(self, folders, *args, **kwargs):
        """
        Populates the given folders with their children. To fetch the root
        folder, do fetchDescendents([InventoryFolder()]) 
        """
        response = self.sendFetchDescendents(folders, *args, **kwargs)
        for folder, folder_response in zip(folders, response['folders']):
            self._populateFolderFromResponse(folder, folder_response)

    def sendFetch(self, items):
        request = {'items': [{
            'item_id': item.id,
            'owner_id': item.owner_id,
        } for item in items]}
        return self.fetchCapability.POST(request)

    def fetch(self, items):
        response = self.sendFetch(items)
        for item, item_response in zip(items, response['items']):
            self._populateItemFromResponse(item, item_response)

    def _populateFolderFromResponse(self, folder, response):
        for k, v in response.items():
            if k == 'agent_id':
                pass # ignore
            elif k == 'categories':
                for subfolder_response in v:
                    subfolder = InventoryFolder()
                    subfolder.parent = folder
                    folder.inventory.append(subfolder)
                    self._populateFolderFromResponse(subfolder, subfolder_response)
            elif k == 'items':
                for item_response in v:
                    item = InventoryItem()
                    item.parent = folder
                    folder.inventory.append(item)
                    self._populateItemFromResponse(item, item_response)
            else:
                setattr(folder, FOLDER_LLSD_ATTR_MAP[k], v)

    def _populateItemFromResponse(self, item, response):
        for k, v in response.items():
            if k == 'agent_id':
                pass # ignore
            elif k == 'permissions':
                for k, v in v.items():
                    setattr(item, k, v)
            elif k == 'sale_info':
                for k, v in v.items():
                    setattr(item, k, v)
            else:
                setattr(item, ITEM_LLSD_ATTR_MAP[k], v)

    def __repr__(self):
        return 'AISv2Client({}, {})'.format(
                self.fetchCapability, self.fetchDescendentsCapability)

def createClient(agent, source = 'inventory'):
    caps = agent.region.capabilities
    if not caps[FETCH_INVENTORY_CAP_NAME]:
        return None
    if source == 'inventory':
        return AISv2Client(caps[FETCH_INVENTORY_CAP_NAME],
                caps[FETCH_INVENTORY_DESCENDENTS_CAP_NAME])
    elif source == 'library':
        return AISv2Client(caps[FETCH_LIBRARY_CAP_NAME], 
                caps[FETCH_LIBRARY_DESCENDENTS_CAP_NAME])
    else:
        raise ValueError('Unknown inventory source {}'.format(source))

