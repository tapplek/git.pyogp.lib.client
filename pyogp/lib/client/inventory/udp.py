"""
Contributors can be viewed at:
http://svn.secondlife.com/svn/linden/projects/2008/pyogp/lib/base/trunk/CONTRIBUTORS.txt 

$LicenseInfo:firstyear=2008&license=apachev2$

Copyright 2009, Linden Research, Inc.

Licensed under the Apache License, Version 2.0.
You may obtain a copy of the License at:
    http://www.apache.org/licenses/LICENSE-2.0
or in 
    http://svn.secondlife.com/svn/linden/projects/2008/pyogp/lib/base/LICENSE.txt

$/LicenseInfo$
"""

# related
from uuid import UUID
from llbase import lluuid

# pyogp.message
from pyogp.lib.base.message.message import Message, Block

# pyogp
from .core import InventoryManager, logger

# ToDo: handle library inventory properly. right now, it's treated as regular inv. store it in self.library_folders
class UDP_Inventory(InventoryManager):
    """
    UDP Inventory Manager. UDP inventory is obsolete and was disabled
    server-side on the main grid Tuesday, April 7, 2015:
    http://wiki.secondlife.com/wiki/Release_Notes/Second_Life_Server/15#15.03.30.300351
    It has been replaced with AISv3, aka, HTTP Inventory
    """

    def __init__(self, agent, settings = None):

        super(UDP_Inventory, self).__init__(agent, settings)

    def sendFetchInventoryRequest(self, folder_id = None):
        """ send a request to the grid for folder attributes """

        raise NotImplemented("sendFetchInventoryRequest")

    def sendFetchLibRequest(self, item_id = None):
        """ send a request to the grid for library items """

        raise NotImplemented("sendFetchInventoryRequest")

    def sendFetchInventoryDescendentsRequest(self, folder_id = None):
        """ send a request to the grid for folder contents """

        packet = Message('FetchInventoryDescendents',
                        Block('AgentData',
                                AgentID = self.agent.agent_id,          # MVT_LLUUID
                                SessionID = self.agent.session_id),     # MVT_LLUUID
                        Block('InventoryData',
                                FolderID = UUID(str(folder_id)),        # MVT_LLUUID
                                OwnerID = self.agent.agent_id,          # MVT_LLUUID
                                SortOrder = 0,                          # MVT_S32, 0 = name, 1 = time
                                FetchFolders = True,                    # MVT_BOOL
                                FetchItems = True))                     # MVT_BOOL

        self.agent.region.enqueue_message(packet)

    def sendFetchLibDescendentsRequest(self, folder_id = None):
        """ send a request to the grid for library items """

        raise NotImplemented("sendFetchLibDescendentsRequest")

    def onFetchInventoryReply(self, packet):

        _agent_id = packet['AgentData'][0]['AgentID']

        for InventoryData_block in packet['InventoryData']:

            _ItemID = InventoryData_block['ItemID']
            _FolderID = InventoryData_block['FolderID']
            _CreatorID = InventoryData_block['CreatorID']
            _OwnerID = InventoryData_block['OwnerID']
            _GroupID = InventoryData_block['GroupID']
            _BaseMask = InventoryData_block['BaseMask']
            _OwnerMask = InventoryData_block['OwnerMask']
            _GroupMask = InventoryData_block['GroupMask']
            _EveryoneMask = InventoryData_block['EveryoneMask']
            _NextOwnerMask = InventoryData_block['NextOwnerMask']
            _GroupOwned = InventoryData_block['GroupOwned']
            _AssetID = InventoryData_block['AssetID']
            _Type = InventoryData_block['Type']
            _InvType = InventoryData_block['InvType']
            _Flags = InventoryData_block['Flags']
            _SaleType = InventoryData_block['SaleType']
            _SalePrice = InventoryData_block['SalePrice']
            _Name = InventoryData_block['Name']
            _Description = InventoryData_block['Description']
            _CreationDate = InventoryData_block['CreationDate']
            _CRC = InventoryData_block['CRC']

            inventory_item = InventoryItem(_ItemID, _FolderID, _CreatorID, _OwnerID, _GroupID, _BaseMask, _OwnerMask, _GroupMask, _EveryoneMask, _NextOwnerMask, _GroupOwned, _AssetID, _Type, _InvType, _Flags, _SaleType, _SalePrice, _Name, _Description, _CreationDate, _CRC)

            self._store_inventory_item(inventory_item)

    def onInventoryDescendents(self, packet):

        if packet['AgentData'][0]['Descendents'] > 0:

            _agent_id = packet['AgentData'][0]['AgentID']
            _folder_id = packet['AgentData'][0]['FolderID']
            _owner_id = packet['AgentData'][0]['OwnerID']
            _version = packet['AgentData'][0]['Version']
            _descendents = packet['AgentData'][0]['Descendents']
            # _descendents is not dealt with in any way here


            if str(packet['ItemData'][0]['ItemID']) != str(lluuid.NULL):

                for ItemData_block in packet['ItemData']:

                    _ItemID = ItemData_block['ItemID']
                    _FolderID = ItemData_block['FolderID']
                    _CreatorID = ItemData_block['CreatorID']
                    _OwnerID = ItemData_block['OwnerID']
                    _GroupID = ItemData_block['GroupID']
                    _BaseMask = ItemData_block['BaseMask']
                    _OwnerMask = ItemData_block['OwnerMask']
                    _GroupMask = ItemData_block['GroupMask']
                    _EveryoneMask = ItemData_block['EveryoneMask']
                    _NextOwnerMask = ItemData_block['NextOwnerMask']
                    _GroupOwned = ItemData_block['GroupOwned']
                    _AssetID = ItemData_block['AssetID']
                    _Type = ItemData_block['Type']
                    _InvType = ItemData_block['InvType']
                    _Flags = ItemData_block['Flags']
                    _SaleType = ItemData_block['SaleType']
                    _SalePrice = ItemData_block['SalePrice']
                    _Name = ItemData_block['Name']
                    _Description = ItemData_block['Description']
                    _CreationDate = ItemData_block['CreationDate']
                    _CRC = ItemData_block['CRC']

                    inventory_item = InventoryItem(_ItemID, _FolderID, _CreatorID, _OwnerID, _GroupID, _BaseMask, _OwnerMask, _GroupMask, _EveryoneMask, _NextOwnerMask, _GroupOwned, _AssetID, _Type, _InvType, _Flags, _SaleType, _SalePrice, _Name, _Description, _CreationDate, _CRC)

                    self._store_inventory_item(inventory_item)

            if str(packet['FolderData'][0]['FolderID']) != str(lluuid.NULL):

                for FolderData_block in packet['FolderData']:

                    _FolderID = FolderData_block['FolderID']
                    _ParentID = FolderData_block['ParentID']
                    _Type = FolderData_block['Type']
                    _Name = FolderData_block['Name']

                    folder = InventoryFolder( _Name, _FolderID, _ParentID, None, _Type, _agent_id)

                    self._store_inventory_folder(folder)


