
"""
Contributors can be viewed at:
http://svn.secondlife.com/svn/linden/projects/2008/pyogp/lib/base/trunk/CONTRIBUTORS.txt 

$LicenseInfo:firstyear=2008&license=apachev2$

Copyright 2009, Linden Research, Inc.

Licensed under the Apache License, Version 2.0.
You may obtain a copy of the License at:
    http://www.apache.org/licenses/LICENSE-2.0
or in 
    http://svn.secondlife.com/svn/linden/projects/2008/pyogp/lib/base/LICENSE.txt

$/LicenseInfo$
"""

# pyogp
from pyogp.lib.client.exc import ResourceError, ResourceNotFound

# pyogp
from .core import InventoryManager, logger

class AIS(InventoryManager):
    """ 
    AIS specific inventory manager. Implements AIS v1 protocol. AISv1 is
    obsolete and was removed from main grid as of January 2011 and replaced
    with AISv2, according to
    http://wiki.secondlife.com/wiki/Linden_Lab_Official:Inventory_API
    """

    def __init__(self, agent, capabilities, settings = None):

        super(AIS, self).__init__(agent, settings)

        self.capabilities = capabilities

    def sendFetchInventoryRequest(self, item_ids = []):
        """ send a request to the grid for inventory item attributes """

        if len(item_ids) == 0:
            logger.warning("sendFetchInventoryRequest requires > 0 item_ids, 0 passed in")
            return
        elif type(item_ids) != list:
            logger.warning("sendFetchInventoryRequest requires a list of item_ids, %s passed in" % (type(item_ids)))
            return            

        cap = self.capabilities['FetchInventory']

        post_body = {'items': [{'item_id': str(item_id)} for item_id in item_ids]}

        custom_headers = {'Accept' : 'application/llsd+xml'}

        try:
            result = cap.POST(post_body, custom_headers)
        except ResourceError as error:
            logger.error(error)
            return
        except ResourceNotFound as error:
            logger.error("404 calling: %s" % (error))
            return

        for item in response['folders']:

            self._store_caps_items(item, folder_id)

    def sendFetchLibRequest(self, folder_id, item_ids = []):
        """ send a request to the grid for library item attributes """

        if len(item_ids) == 0:
            logger.warning("sendFetchLibRequest requires > 0 item_ids, 0 passed in")
            return
        elif type(item_ids) != list:
            logger.warning("sendFetchLibRequest a list of item_ids, %s passed in" % (type(item_ids)))
            return            

        cap = self.capabilities['FetchLib']

        post_body = {'items': [{'item_id': str(item_id)} for item_id in item_ids]}

        custom_headers = {'Accept' : 'application/llsd+xml'}

        try:
            result = cap.POST(post_body, custom_headers)
        except ResourceError as error:
            logger.error(error)
            return
        except ResourceNotFound as error:
            logger.error("404 calling: %s" % (error))
            return

        for item in response['folders']:

            self._store_caps_items(item, folder_id, 'library')

    def sendFetchInventoryDescendentsRequest(self, folder_id):
        """ send a request to the server for inventory folder contents """

        cap = self.capabilities['WebFetchInventoryDescendents']

        post_body = {'folders': [{'folder_id': str(folder_id), 'owner_id': str(self.agent.agent_id)}]}

        custom_headers = {'Accept' : 'application/llsd+xml'}

        try:
            response = cap.POST(post_body, custom_headers)
        except ResourceError as error:
            logger.error(error)
            return
        except ResourceNotFound as error:
            logger.error("404 calling: %s" % (error))
            return

        '''
        Response shape

        {'folders':[ {'category': CATEGORY_SHAPE,
                'categories': [CATEGORY_SHAPE,],
                'items': [ITEM_SHAPE,] }]}
        '''

        for member in response['folders']:

            if 'category' in member:
                self._store_inventory_folder(member['category'])

            if 'categories' in member:
                [self._store_inventory_folder(folder_info) for folder_info in member['categories']]

            # pass a tuple of (item_info, category_id)
            if 'items' in member:
                self._store_caps_items(member['items'], member['category']['category_id'])

    def _store_caps_items(self, items, folder_id, destination = 'inventory'):
        """ transform an AIS caps response's inventory items and folder_id to InventoryItems and store it """

        for item in items:
            inventory_item = InventoryItem(item['item_id'], folder_id, item['permissions']['creator_id'], item['permissions']['owner_id'], item['permissions']['group_id'], item['permissions']['base_mask'], item['permissions']['owner_mask'], item['permissions']['group_mask'], item['permissions']['everyone_mask'], item['permissions']['next_owner_mask'], None, item['asset_id'], item['type'], item['inv_type'], item['flags'], item['sale_info']['sale_type'], item['sale_info']['sale_price'], item['name'], item['desc'], item['created_at'], None, item['permissions']['last_owner_id'])

            self._store_inventory_item(inventory_item, destination)

    def sendFetchLibDescendentsRequest(self, folder_id):
        """ send a request to the server for folder contents """

        cap = self.capabilities['FetchLibDescendents']

        post_body = {'folders': [{'folder_id': str(folder_id), 'owner_id': self.settings.ALEXANDRIA_LINDEN}]}

        custom_headers = {'Accept' : 'application/llsd+xml'}

        try:
            response = cap.POST(post_body, custom_headers)
        except ResourceError as error:
            logger.error(error)
            return
        except ResourceNotFound as error:
            logger.error("404 calling: %s" % (error))
            return

        '''
        Response shape

        {'folders':[ {'category': CATEGORY_SHAPE,
                 'categories': [CATEGORY_SHAPE,],
                 'items': [ITEM_SHAPE,] }]}
        '''

        for member in response['folders']:

            if 'category' in member:
                self._store_inventory_folder(member['category'], 'library')

            if 'categories' in member:
                [self._store_inventory_folder(folder_info, 'library') for folder_info in member['categories']]

            if 'items' in member:
                self._store_caps_items(member['items'], member['category']['category_id'], 'library')


